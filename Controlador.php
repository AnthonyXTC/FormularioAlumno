<?php

require_once './database.php';

class Controlador {

    private $bd;

    public function __construct() {
        $this->bd = new database();
    }
    
    public function insertarAlumno($Nombre, $A_paterno, $A_materno, $Comunidad, $Calle, $Colonia, $idCarreras, $Tel, $TelCel, $TelOfi, $CURP, $CP, $Int, $Ext, $NIA, $RFC, $No_SS, $No_CTA, $Cuatrimestre, $idArea, $Municipio) {
        $existe = "";
        $consulta = $this->bd->consultar("Select CURP from alumno where CURP = '" . $CURP . "'");
        if (count($consulta) > 0) {
            foreach ($consulta as $c) {
                if ($CURP == $c['CURP']) {
                    $existe = 1;
                }
            }
        } else {
            $existe = 0;
        }
        if ($existe == 1) {
            return "uno";
        } else if ($this->bd->insertar("alumno", "(idAlumno, Nombre, A_paterno, A_materno, Comunidad,Calle, Colonia, No_ext, No_int, CP, Tel, Telcel, Telofi, CURP, NIA, RFC, No_SS, No_CTA,idMunicipio, Cuatrimestre, idCarreras, idArea)"
                        , "(null, '" . $Nombre . "','" . $A_paterno . "','" . $A_materno . "','" . $Comunidad . "','" . $Calle . "'" . ",'" . $Colonia . "'," . $Ext . "," . $Int . "," . $CP . "," . $Tel . "," . $TelCel . "," . $TelOfi . ""
                        . ",'" . $CURP . "','" . $NIA . "','" . $RFC . "'," . $No_SS . "," . $No_CTA . "," . $Municipio . ",'" . $Cuatrimestre . "'," . $idCarreras . "," . $idArea . ")")) {
            
        } else {
            return "cero";
        }
    }

    public function ModalPerfilVoc($idAlumno) {
        $alumnoLP = $this->bd->consultar("select concat(alumno.Nombre,' ', alumno.A_Paterno,' ',alumno.A_Materno) as nombreAlumno, 
  alumno.Tel, alumno.Telcel, alumno.Telofi, alumno.Comunidad, alumno.Colonia, alumno.Calle, alumno.No_int, alumno.No_ext, alumno.CP,
  alumno.CURP,alumno.NIA,alumno.RFC,alumno.No_SS,alumno.No_CTA,
carreras.Nombre as carrera,
area.Nombre as Area,
municipio.Nombre as municipio,
estado.Nombre as estado
from alumno
join carreras 
on carreras.idCarreras = alumno.idCarreras
join municipio 
on  municipio.idMunicipio = alumno.idMunicipio
join estado
on  estado.idEstado = municipio.idEstado
join area 
on area.idArea = alumno.idArea
where alumno.idAlumno = '" . $idAlumno . "'");

        $alumnoUni = $this->bd->consultar("Select universidad.Nombre as Universidad
from universidad
join carreras
on carreras.idUniversidad = universidad.idUniversidad
join alumno
on alumno.idCarreras = carreras.idCarreras
where alumno.idAlumno = '" . $idAlumno . "'");


        foreach ($alumnoLP as $a) {
            
        }
        foreach ($alumnoUni as $u) {
            
        }

        echo '  <div class="container-fluid row">
                                                                                       <!--                                               Perfil Alumno-->
                                            <div>
                                                <!--                                                informacion personal-->

                                                <div class="panel panel-primary">
                                                    <div class="panel-heading">
                                                        <h4 class="h4 panel-title ">Informacion Personal</h4>
                                                    </div>
                                                    <div class="panel-body text-left">
                                                        <span><strong>Nombre: </strong>' . $a['nombreAlumno'] . '</span><br>
                                                             <span><strong>Universidad: </strong>' . $u['Universidad'] . '</span><br>
                                                        <span><strong>Carrera: </strong>' . $a['carrera'] . '</span><br>
                                                           <span><strong>Area: </strong>' . $a['Area'] . '</span><br>
                                                                <span><strong>CURP: </strong>' . $a['CURP'] . '</span><br>
                                                                     <span><strong>NIA: </strong>' . $a['NIA'] . '</span><br>
                                                                          <span><strong>RFC: </strong>' . $a['RFC'] . '</span><br>
                                                                               <span><strong>No_SS: </strong>' . $a['No_SS'] . '</span><br>
                                                                                   <span><strong>No_CTA: </strong>' . $a['No_CTA'] . '</span><br>
                                                        
                                                    </div>
                                                </div>
                                                <!--       Telefonos      -->
                                                <div class="panel panel-info">
                                                    <div class="panel-heading">
                                                        <h4 class="h4 panel-title ">Telefonos</h4>
                                                    </div>
                                                    <div class="panel-body text-left">
                                                        <span><strong>Casa :</strong>' . $a['Tel'] . '</span>
                                                        <span><strong>Celular :</strong>' . $a['Telcel'] . '</span>
                                                                 <span><strong>Oficina :</strong>' . $a['Telofi'] . '</span>
                                                            
                                                    </div>
                                                </div>

                                                                                             <!--      Lugar de Residencia     -->
                                                <div class="panel panel-warning">
                                                    <div class="panel-heading">
                                                        <h4 class="h4 panel-title ">Domicilio</h4>
                                                    </div>
                                                    <div class="panel-body text-left">
                                                        <span><strong>Estado: </strong>' . $a['estado'] . '</span>
                                                        <span><strong>Municipio: </strong>' . $a['municipio'] . '</span>
                                                        <span><strong>Localidad: </strong>' . $a['Comunidad'] . '</span><br>
                                                        <span><strong>Calle: </strong>' . $a['Calle'] . '</span>
                                                        <span><strong>Colonia: </strong></span>' . $a['Colonia'] . '<br>
                                                        <span><strong>No. Exterior: </strong>' . $a['No_ext'] . '</span>
                                                        <span><strong>No. Interior: </strong>' . $a['No_int'] . '</span>
                                                        <span><strong>Codigo Postal: </strong>' . $a['CP'] . '</span>
                                                    </div>
                                                </div>
                                                                                         
                                            </div>

                                        </div>';
    }

}
