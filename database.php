<?php

class database {

    private $host;
    private $user;
    private $password;
    private $database;
    private $conn;

    public function __construct() {

//    //        //hostinger
//             $this->host = "mysql.hostinger.mx";
//            $this->user = "u798767772_tony";
//            $this->password = "051243aa";
//            $this->database = "u798767772_form";
////        localhost  
        $this->host = "localhost";
        $this->user = "root";
        $this->password = "";
        $this->database = "test";
    }

    private function abrirConexion() {
        $this->conn = new mysqli($this->host, $this->user, $this->password, $this->database);

        if ($this->conn->connect_errno) {
            die("Fallo conexion: " . $this->conn > connect_errno . ")");
        }
        $this->conn->query("Set names 'utf8'");
    }

    public function cerrarConexion() {
        $this->conn->close();
    }

    public function consultar($sql) {
        $this->abrirConexion();
        $re = $this->conn->query($sql);
        $registros = array();
        if (!$re) {
            printf("Error:%s\n" + mysqli_error($this->conn));
            exit();
        }
        if (count($re) > 0) {

            while ($reg = mysqli_fetch_array($re)) {
                $registros[] = $reg;
            }
        }

        return $registros;
    }

    public function insertar($tabla, $campos, $valores) {
        if ($this->conn == false) {
            $this->abrirConexion();
        }
        if ($this->conn->query("insert into " . $tabla . " " . $campos . " values " . $valores)) {
            $this->cerrarConexion();
            return true;
        } else {
            return "insert into " . $tabla . " " . $campos . " values " . $valores;
            $this->cerrarConexion();
            //return false;
        }
    }

    public function actualizar($tabla, $campos, $condicion) {
        if ($this->conn == false) {
            $this->abrirConexion();
        }
        if ($this->conn->query("update " . $tabla . " set " . $campos . " where " . $condicion)) {
            return true;
        } else {
            return false;
        }
        $this->cerrarConexion();
    }

    public function eliminar($tabla, $condicion) {
        if ($this->conn == false) {
            $this->abrirConexion();
        }
        echo $sql = "DELETE FROM " . $tabla . " WHERE " . $condicion;
        if ($this->conn->query($sql)) {
            return true;
        } else {
            $this->cerrarConexion();
            return false;
        }
    }

}
