<?php

require './Controlador.php';
$control = new Controlador();

$respuesta = "";


if (isset($_POST['No_ext']) && !empty($_POST['No_ext'])) {
    $Ext = $_POST['No_ext'];
} else {
    $Ext = 0;
}
if (isset($_POST['No_int']) && !empty($_POST['No_int'])) {
    $Int = $_POST['No_int'];
} else {
    $Int = 0;
}
if (isset($_POST['CP']) && !empty($_POST['CP'])) {
    $CP = $_POST['CP'];
} else {
    $CP = 0;
}
if (isset($_POST['Area']) && !empty($_POST['Area'])) {
    $idArea = $_POST['Area'];
} else {
    $idArea = "N/A";
}

$Nombre = "";
$A_paterno = "";
$A_materno = "";
$Comunidad = "";
$Calle = "";
$Colonia = "";
$idCarreras = "";
$Tel = "";
$TelCel = "";
$TelOfi = "";
$CURP = "";
$NIA = "";
$RFC = "";
$No_SS = "";
$No_CTA = "";
$Cuatrimestre = "";
$Estado = "";
$Municipio = "";

//Informacion Personal
$Nombre = mb_strtoupper($_POST['Nombre'], 'utf-8');
$A_paterno = mb_strtoupper($_POST['A_paterno'], 'utf-8');
$A_materno = mb_strtoupper($_POST['A_materno'], 'utf-8');
$Comunidad = mb_strtoupper($_POST['Comunidad'], 'utf-8');
$Calle = mb_strtoupper($_POST['Calle'], 'utf-8');
$Colonia = mb_strtoupper($_POST['Colonia'], 'utf-8');
$idUniversdiad = $_POST['Universidad'];
$idCarreras = $_POST['Carrera'];
$Tel = $_POST['telcasa'];
$TelCel = $_POST['telcel'];
$TelOfi = $_POST['telofi'];
$CURP = $_POST['CURP'];
$NIA = $_POST['NIA'];
$RFC = $_POST['RFC'];
$No_SS = $_POST['No_SS'];
$No_CTA = $_POST['No_CTA'];
$Cuatrimestre = $_POST['Cuatrimestre'];
//$idArea = $_POST['Area'];
$Estado = $_POST['estado-residencia'];
$Municipio = $_POST['municipio-residencia'];

//echo
//"Recibi desde ajax: <br>"
// . "Nombre Completo: " . $Nombre . "  " . $A_paterno . "  " . $A_materno
// . "<br>Estado de Residencia: " . $Estado . "Municipio de Residencia: " . $Municipio
// . "<br> Localidad: " . $Comunidad . " Calle: " . $Calle . "  Colonia: " . $Colonia
// . "<br>Numero Exterior: " . $Ext . "Numero Interior: " . $Int . "Codigo Postal: " . $CP
// . "<br>Tel Casa: " . $Tel . "  Tel Cel: " . $TelCel . "  Tel Ofi: " . $TelOfi
// . "<br> idUniversidad: " . $idUniversdiad . " idCarrera: " . $idCarreras . " Area: " . $idArea . " Cuatrimestre: " . $Cuatrimestre
// . "<br>CURP: " . $CURP . " NIA: " . $NIA . "  RFC: " . $RFC . " No_SS: " . $No_SS . "  No_CTA: " . $No_CTA
//;


$grats = $_POST['Nombre'] . ' ' . $_POST['A_paterno'];


$respuesta = $control->insertarAlumno($Nombre, $A_paterno, $A_materno, $Comunidad, $Calle, $Colonia, $idCarreras, $Tel, $TelCel, $TelOfi, $CURP, $CP, $Int, $Ext, $NIA, $RFC, $No_SS, $No_CTA, $Cuatrimestre, $idArea, $Municipio);

if ($respuesta == "uno") {
    echo'   <div id="overlay">
                          <div class="div container">
                                <div class="panel panel-primary">
                                    <div class="panel-heading text-center">

                                        <h1 style="display: inline-block"><strong>Lo sentimos :&apos;(</strong></h1>
                                    </div>
                                    <div class="panel-body">
                                        <h2>Los datos de alumno que ingresaste ya se encuentran registrados.</h2>
                                        <div class="form-group">
                                            <button class="btn btn-primary pull-right"  id="refresh">Entendido</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  ';
} else if ($respuesta == "cero") {
    echo'   <div id="overlay" class"row">
                            <div class="div container">
                                <div class="panel panel-primary">
                                    <div class="panel-heading text-center">
                                        
                                        <h1 style="display: inline-block"><strong>Lo sentimos :&apos;(</strong></h1>
                                    </div>
                                    <div class="panel-body">
                                        <h3>No se ha podido completar el registro, porfavor intentelo más tarde.</h3>
                                                                             <div class="form-group">
                                        <button class="btn btn-primary pull-right"  id="refresh">Volver a intentar</button>
                                        </div>
                                        </div>
                                </div>
                            </div>
                        </div>';
} else {
    echo' <div id="overlay">
                <div class="div container">
                    <div class="panel panel-success">
                        <div class="panel-heading text-center">
                            <i class="fa fa-check-square-o fa-3x">&nbsp;</i>
                            <h1 style="display: inline-block"><strong>&nbsp;Registro Completado!</strong></h1>
                        </div>
                        <div class="panel-body">
                            <h3>El registro se completo satisfactoriamente. </h3>
                            
                            <span>Muchas gracias<strong>&nbsp' . $grats . '&nbsp</strong>no hay nada más que hacer por momento.</span>
                                                        </div>
                        <div class="panel-footer">                              
                            <div class="form-group">        
                                <button class="btn btn-success btn-lg" id="adelante">Volver al inicio!</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
}




