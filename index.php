<?php
header('Content-Type: text/html; charset=UTF-8');
include_once './contenido.php';
$mostrar = new contenido();
?>
<html lang="es">
    <head>
        <title>Formulario</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="icon" href="img/favicon.png" type="image/x-icon">
        <link rel="stylesheet" href="css/Style.css"/>
        <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.min.css" />
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/animate.css">
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
    </head>
    <body id="bodyRegistro">

        <div id="containerInfo" class="container-fluid">
            <!--  HEADER -->
            <header class=" row ">
                <h1 class="h1 text-center">FORMULARIO UTT</h1>
            </header>
            <!--  Seccion de inicio -->
            <section id="inicio" class="row">
                <div class="container">
                    <h3>Elige la accion que deseas realizar.</h3>
                    <hr class="hr-divider">
                    <ul class="list-inline">
                        <li><div class="form-group">
                                <button class="btn  btn-lg btn-success animated  " id="btn-Registro">Nuevo Registro</button>
                            </div>

                        </li>
                        <li>
                            <div class="form-group">
                                <button class=" btn btn-lg  btn-primary animated" id="btn-Consultar">Consultar Registros</button>
                            </div>

                        </li>
                    </ul>
                </div>
            </section>

            <!--Seccion Registro -->
            <section id="Registro" class="container text-center animated fadeIn" >
                <button type="button" class="close backbtn"><span aria-hidden="true"><i class="fa fa-reply"></i></span> ATRAS</button>
                <div class="clearfix"></div>
                <h2 class="text-center">REGISTRO DE INFORMACION</h2>
                <div id="bandera">         

                </div>
                <form  id="RegistroAlumno" role="form"  class="" >

                    <div class="pull-left  container-fluid col-xs-12 col-sm-6 col-lg-6">
                        <div class=" container-fluid">

                            <!-- I n f o r m a c i o n     P e r s o n a l   -->

                            <div class="form-group" id="personal">
                                <h2>Información</h2>

                                <div class="form-group">
                                    <!-- N o m b r e-->
                                    <div> 
                                        <input required pattern="[A-Za-z\sÁÉÍÓÚ-áéíóú{3,50}" title="Debes escribir tu nombre" required class="form-control" type="text" placeholder="Nombre(s)" name="Nombre" id="Nombre"  >   
                                        <span class="help-block"></span>
                                    </div>
                                    <!-- A P A T E R N O -->
                                    <div>
                                        <input required pattern="[A-Za-z\sÁÉÍÓÚ-áéíóú{3,50}" title="Debes escribir tu apellido" required class="form-control" type="text" placeholder="A. Paterno" name="A_paterno" id="A_paterno"> 
                                        <span class="help-block"></span>
                                    </div>
                                    <!-- A M A T E R N O -->
                                    <div>
                                        <input required pattern="[A-Za-z\sÁÉÍÓÚ-áéíóú{3,50}" title="Debes escribir tu apellido" class="form-control" type="text" placeholder="A. Materno" name="A_materno" id="A_materno"> 
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                            <!-- L U G A R      D E      R E S I D E N C I A  -->
                            <div id="residencia">
                                <h2>Domicilio</h2>
                                <div class="form-group">
                                    <!--  Estado -->
                                    <div>
                                        <select name="estado-residencia" id="estado-residencia"  onchange="cambiarMunicipio($('#estado-residencia').val())" class="form-control">
                                            <option selected="selected" disabled="">Estado</option>
                                            <?PHP
                                            $selectEstados = $mostrar->SelectEstados();
                                            ?>                               
                                        </select>
                                        <span class="help-block"></span>
                                    </div>
                                    <!--    Municipio      -->
                                    <div>
                                        <select name="municipio-residencia" id="municipio-residencia" class="form-control">
                                            <option selected="selected" disabled="" >Municipio</option>
                                        </select>
                                        <span class="help-block"></span>
                                    </div>
                                    <!--   Localidad    -->
                                    <div>
                                        <input  title="Ingresa tu localidad" required pattern="[A-Za-z\sÁÉÍÓÚ-áéíóú{3,50}" name="Comunidad" class="form-control" type="text" placeholder="Ciudad/Localidad" id="Comunidad"> 
                                        <span class="help-block"></span>
                                    </div>
                                    <!--    Calle      -->
                                    <div>
                                        <input title="Ingresa tu calle"  required pattern="[A-Za-z\sÁÉÍÓÚ-áéíóú{3,50}" name="Calle" class="form-control" type="text" placeholder="Calle" id="Calle"> 
                                        <span class="help-block"></span>
                                    </div>
                                    <!--   Colonia       -->
                                    <div>
                                        <input title="Ingresa tu colonia"  required pattern="[A-Za-z\sÁÉÍÓÚ-áéíóú{3,50}" name="Colonia" class="form-control" type="text" placeholder="Colonia" id="Colonia">
                                        <span class="help-block"></span>
                                    </div>
                                    <!--     nexterior    -->
                                    <div>
                                        <input pattern="[0-9]{1,9}" required title="Solo puedes ingresar numeros" name="No_ext" min="1"  class="form-control" type="number" placeholder="No. Exterior" id="No_ext">
                                    </div>
                                    <!--     ninterior     -->
                                    <div>
                                        <input pattern="[0-9]{1,9}" title="Solo puedes ingresar numeros" name="No_int" min="1"  class="form-control" type="number" placeholder="No. Interior" id="No_int">
                                    </div>
                                    <!--   C P       -->
                                    <div>
                                        <input pattern="[0-9]{1,5}" name="CP" min="0"   class="form-control" type="number" placeholder="Codigo Postal" id="CP">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pull-left container-fluid  col-xs-12 col-sm-6 col-lg-6">
                        <div class=" container-fluid">
                            <!--  T E L E F O N O S    -->
                            <div id="telefonos">
                                <h2>Telefono</h2>
                                <input required pattern="[0-9]{7,10}"  title="De 7 a 10 Digitos"  class="form-control " type="tel" placeholder="Casa" name="telcasa" id="telcasa">                  
                                <input required pattern="[0-9]{10}" title="10 Digitos"   class="form-control" type="tel" placeholder="Celular (EJ: 238XXX...)" name="telcel" id="telcel">
                                <input required pattern="[0-9]{7,10}" title="De 7 a 10 Digitos"   class="form-control" type="tel" placeholder="Oficina (EJ: 238XXX...)" name="telofi" id="telofi">
                            </div>

                            <!-- D A T O S  P E R S O N A L E S -->
                            <div id="datospersonales">
                                <h2>Datos Personales</h2>
                                <div class="form-group">
                                    <!--  CURP  -->
                                    <div>
                                        <input type="text"  required placeholder="CURP" id="CURP" name="CURP" class="form-control"/>
                                    </div>
                                    <!--  NIA -->
                                    <div>
                                        <input type="number" required class="form-control" id="NIA" name="NIA"  placeholder="NIA"/>
                                    </div>
                                    <!--  RFC -->
                                    <div>
                                        <input type="number" required class="form-control" id="RFC" name="RFC" placeholder="RFC"/>
                                    </div>
                                    <!-- No. SS-->
                                    <div>
                                        <input type="number" required class="form-control"  name="No_SS" id="No_SS"   placeholder="No. SS" />
                                    </div>
                                    <!-- No. Cuenta -->
                                    <div>
                                        <input type="number"  required  class="form-control" id="No_CTA" name="No_CTA" placeholder="No. Cta" />
                                    </div>
                                </div>
                            </div>
                            <!-- D A T O S     E S T U D I O  -->
                            <div id="datosestudio">
                                <h2>Datos Estudio</h2>
                                <div class="form-group">
                                    <!--  UNIVERSIDAD  -->
                                    <div>
                                        <select name="Universidad" id="Universidad" class="form-control" onchange="cambiarCarreras($('#Universidad').val())" >
                                            <option selected="selected" disabled="" >Universidad</option>
                                            <?php
                                            $selectUniversidades = $mostrar->SelectUniversidades();
                                            ?>
                                            
                                        </select>
                                         <span class="help-block"></span>
                                    </div>
                                    <!--  CARRERA  -->
                                    <div>
                                        <select name="Carrera" id="Carrera" class="form-control"  onchange="cambiarAreas($('#Carrera').val())">
                                            <option selected="selected" disabled="" >Carrera/Division</option>

                                        </select>
                                         <span class="help-block"></span>
                                    </div>

                                    <!--  AREA  -->
                                    <div>
                                        <select name="Area" id="Area" class="form-control" >
                                            <option selected="selected" disabled="" >Area/Facultad</option>
                                        </select>
                                         <span class="help-block"></span>
                                    </div>
                                    <!-- CUATRIMESTRE  -->
                                    <div>
                                        <select name="Cuatrimestre" id="Cuatrimestre" class="form-control" >
                                            <option selected="selected" disabled="" >Cuatrimestre</option>
                                            <option value="PRIMERO">PRIMERO</option>
                                            <option value="SEGUNDO">SEGUNDO</option>
                                            <option value="TERCERO">TERCERO</option>
                                            <option value="CUARTO">CUARTO</option>
                                            <option value="QUINTO">QUINTO</option>
                                            <option value="SEXTO">SEXTO</option>
                                            <option value="SEPTIMO">SEPTIMO</option>
                                            <option value="OCTAVO">OCTAVO</option>
                                            <option value="NOVENO">NOVENO</option>
                                            <option value="DECIMO">DECIMO</option>
                                            <option value="ONCEAVO">ONCEAVO</option>
                                        </select>
                                         <span class="help-block"></span>

                                    </div>

                                </div>
                            </div>


                        </div>

                    </div>


                    <div class="form-group">
                        <button class="btn btn-block btn-lg btn-success "   id="siguiente" type="submit" name="siguiente">REGISTRAR MIS DATOS</button> 
                    </div>

                </form>

            </section>

            <!--Seccion Consultar Registros -->
            <section id="ConsultarRegistros"  class="animated pulse">
                <button type="button" class="close backbtn"><span aria-hidden="true"><i class="fa fa-reply"></i></span> ATRAS</button>
                <div class="row text-center">
                    <h3 class="text-center">ALUMNOS REGISTRADOS</h3>
                </div>

                <div class="panel-body table-responsive">
                    <div class="scrollable">
                        <table id="tableReg" class="table table-bordered table-hover table-condensed">
                            <thead>
                                <tr class="bg-info">
                                    <th class="text-center">NOMBRE</th>
                                    <th class="text-center">UNIVERSIDAD</th>
                                    <th class="text-center">CARRERA</th>
                                    <th class="text-center">AREA</th>
                                    <th class="text-center">Cuatrimestre</th>
                                    <th class="text-center">VER MÁS</th>
                                </tr>
                            </thead>
                            <tbody> 

                                <!-- CONTENIDO/ LISTA ALUMNOS --> 
                                <?php
                                $consultaAlumno = $mostrar->consultaAlumno();
                                echo $consultaAlumno;
                                ?>

                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="clearfix"></div>
            </section>


            <!-- MODAL PERFIL -->
            <div class="modal fade bs-example-modal-sm" id="modalPerfill" tabindex="-1" role="dialog" aria-labelledby="modalPerfill">
                <div class="modal-dialog modal-md" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h3 class="modal-title" id="modalPerfill">INFORMACION DEL ALUMNO</h3>

                        </div>
                        <div class="modal-body"> 
                            <!-- BODY MODAL -->
                            <!--                                   Contenido-->
                        </div>
                        <div class="modal-footer">

                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- FIN MODAL PERFIL -->

            <!--    Seccion ERROR     -->
            <!--            <div id="" class="jumbotron">
                            <div class="">
                                <div class=" ">
                                    <div class="container">
                                        <img class="" src="img/pandaerror.png">
                                    </div>
                                    <div class="">No se ha encontrado la página</div>
                                    <div class="">Pero esto siempre lo vas a encontrar: la
                                        <a href="https://www.bing.com/?cc=mx">página principal de Bing.</a>
                                    </div>
                                    <div class="">O prueba una de estas búsquedas de
                                        <a href="https://www.bing.com/images/search?FORM=FDNF&q=papel%20tapiz%20de%20gatitos">papel tapiz de gatitos</a>,
                                        <a href="https://www.bing.com/images/search?FORM=FDNF&q=GIF%20de%20animalitos">GIF de animalitos</a>,
                                        <a href="https://www.bing.com/images/search?FORM=FDNF&q=perros%20panda">perros panda</a>.
                                    </div>
                                </div>
                            </div>
                        </div>-->







            <!-- Footer & Copy -->
            <footer class=" row  negro container-fluid">
                <span class="copyright pull-left"><?php echo date("Y"); ?> Copyright © Cenit Enterprise | All Rights Reserved</span> 
            </footer>
        </div>
        <script type="text/javascript" src="js/Registro.js"></script>
        <script type="text/javascript" src="js/tablesorter.min.js"></script>

    </body>
</html>
