<?php

header("Content-Type: text/html;charset=utf-8");

//Incluimos database 
include_once './database.php';



class contenido {
    

private $data;

public function __construct() {

$this->data = new database();
}





public function SelectEstados() {

$re = $this->data->consultar("SELECT * FROM  estado ORDER BY  estado.Nombre ASC ");
$mostrar = "";
foreach ($re as $e) {
echo "<option value='" . $e['idEstado'] . "'> " . $e['Nombre'] . "</option>";
}
return $mostrar;
}

public function SelectCarreras() {

$carreras = $this->data->consultar("SELECT * FROM  carreras ORDER BY  carreras.Nombre ASC ");
$mostrar = "";
foreach ($carreras as $e) {
echo "<option value='" . $e['idCarreras'] . "'> " . $e['Nombre'] . "</option>";
}
return $mostrar;
}

public function SelectUniversidades() {

$carreras = $this->data->consultar("SELECT * FROM  universidad ORDER BY  universidad.Nombre ASC ");
$mostrar = "";
foreach ($carreras as $e) {
echo "<option value='" . $e['idUniversidad'] . "'> " . $e['Nombre'] . "</option>";
}
return $mostrar;
}

public function consultaAlumno() {
$consulta = "";
$consulta = $this->data->consultar("Select universidad.Nombre as Universidad,
concat(alumno.Nombre,' ', alumno.A_paterno,' ',alumno.A_materno) as nombreAlumno, alumno.idAlumno,
area.Nombre as Area,
carreras.Nombre as Carrera,
alumno.Cuatrimestre as Cuatrimestre

from universidad

join carreras as carrera1
on carrera1.idUniversidad = universidad.idUniversidad

join alumno
on alumno.idCarreras = carrera1.idCarreras

join carreras as carrera2
on carrera2.idCarreras = alumno.idCarreras

join area 
on area.idArea = alumno.idArea

join carreras
on carreras.idCarreras = alumno.idCarreras;");
$registros = "";
foreach ($consulta as $c) {
echo $registros = ' <tr>                                                  
                                                    <td class="text-justify">' . mb_strtoupper($c['nombreAlumno'], 'utf-8'). '</td>
                                                         <td class="text-justify">' .  mb_strtoupper($c['Universidad'], 'utf-8') . '</td>
                                                              <td class="text-justify">' .  mb_strtoupper($c['Carrera'], 'utf-8') . '</td>
                                                                   <td class="text-justify">' .  mb_strtoupper($c['Area'], 'utf-8' ). '</td>
                                                                         <td class="text-center">' . mb_strtoupper($c['Cuatrimestre'], 'utf-8') . '</td>
                                                    <td class="text-center">  <!--EL BOTON DISPARA EL MODAL -->
                                                        <input type="hidden" value="' . $c['idAlumno'] . '"/>
                                                        <button type="button" class="btn bg-success" onclick="perfilAlumnoVoc(' . $c['idAlumno'] . ')" data-toggle="modal" data-target="#modalPerfill">
                                                            <span class="glyphicon glyphicon glyphicon-user" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Click Aqui Para Ver El Perfil Del Alumno"></span>
                                                        </button></td>
                                                                                                    </tr>';

}

}

}
