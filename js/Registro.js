var n = 60;
$(document).ready(function () {
//    $(document).on("contextmenu", function () {
//        return false;
//    });
    $(function () {
        $('#tableReg').tablesorter();
    });
    $("span.help-block").hide();

    //El boton desencadena la accion
    $('#RegistroAlumno').submit(function (e) {
        e.preventDefault();

     
        verificar();

    });
    $(document).on("click", "#refresh", function () {
        window.location.reload();
    });
});

function setDisabled() {
    $('#adelante').prop("disabled", false);
}


function verificar() {

    var v1 = 0, v2 = 0, v3 = 0, v4 = 0, v5 = 0, v6 = 0, v7 = 0, v8 = 0, v9 = 0, v10 = 0, v11 = 0, v12 = 0, v13 = 0, v14 = 0, v15 = 0, v16 = 0,v17 = 0;
    v1 = validarDatos('Nombre');
    v2 = validarDatos('A_paterno');
    v3 = validarDatos('A_materno');
    v4 = validarDatos('Comunidad');
    v5 = validarDatos('Calle');
    v6 = validarDatos('Colonia');

    v7 = validarDatos('CURP');
    v8 = validarDatos('NIA');
    v9 = validarDatos('RFC');

    v10 = validarDatos('No_SS');
    v11 = validarDatos('No_CTA');
    v12 = validarDatos('Universidad');
    v13 = validarDatos('Carrera');
    v14 = validarDatos('Area');
    v15 = validarDatos('Cuatrimestre');
    
    v16 = validarDatos('municipio-residencia');
    v17 = validarDatos('estado-residencia');
    if (v1 === false || v2 === false || v3 === false || v4 === false || v5 === false || v6 === false || v7 === false || v8 === false || v9 === false || v10 === false
            || v11 === false || v12 === false || v13 === false || v14 === false || v15 === false || v16 === false || v17===false)
    {
        colocarAlerta(2, "Verifica que hayas llenando todos los campos correctamente");
    }
    else {
        enviarDatos();
    }
}

function validarDatos(campo) {
    // V A L I D A D C I O N E S  N O M B R E
    if (campo === 'Nombre') {

        var nombre = $('#Nombre').val().trim();  //Se obtiene el valor del campo y Se eliminan espacios en blanco con trim()
        if (nombre === '') {
            $("#glypcn" + campo).remove();
            $('#' + campo).parent().attr("class", "form-group has-error has-feedback");
            $('#' + campo).parent().children('span').text("Debes ingresar tu Nombre").show();
            $('#' + campo).parent().append("<span id='glypcn" + campo + "' class='glyphicon glyphicon-remove form-control-feedback'></span>");
            $('#' + campo).val("");
            $('#' + campo).focus();
            return false;
        }
        else {
            $("#glypcn" + campo).removeClass('form-group has-error has-feedback');
            $('#' + campo).parent().attr("class", "form-group has-success has-feedback");
            $('#' + campo).parent().children('span').hide();
            $('#' + campo).parent().append("<span id='glypcn" + campo + "' class='glyphicon glyphicon-ok form-control-feedback'></span>");
            return true;
        }
    }
    // V A L I D A D C I O N E S  A P A T E R N O
    if (campo === 'A_paterno') {

        var apaterno = $('#A_paterno').val().trim();  //Se obtiene el valor del campo y Se eliminan espacios en blanco con trim()
        if (apaterno === '') {
            $("#glypcn" + campo).remove();
            $('#' + campo).parent().attr("class", "form-group has-error has-feedback");
            $('#' + campo).parent().children('span').text("Debes ingresar tu apellido").show();
            $('#' + campo).parent().append("<span id='glypcn" + campo + "' class='glyphicon glyphicon-remove form-control-feedback'></span>");
            $('#' + campo).val("");
            $('#' + campo).focus();
            return false;
        }
        else {
            $("#glypcn" + campo).removeClass('form-group has-error has-feedback');
            $('#' + campo).parent().attr("class", "form-group has-success has-feedback");
            $('#' + campo).parent().children('span').hide();
            $('#' + campo).parent().append("<span id='glypcn" + campo + "' class='glyphicon glyphicon-ok form-control-feedback'></span>");
            return true;
        }
    }
    // V A L I D A D C I O N E S  A M A T E R N O
    if (campo === 'A_materno') {

        var amaterno = $('#A_materno').val().trim();  //Se obtiene el valor del campo y Se eliminan espacios en blanco con trim()
        if (amaterno === '') {
            $("#glypcn" + campo).remove();
            $('#' + campo).parent().attr("class", "form-group has-error has-feedback");
            $('#' + campo).parent().children('span').text("Debes ingresar tu apellido").show();
            $('#' + campo).parent().append("<span id='glypcn" + campo + "' class='glyphicon glyphicon-remove form-control-feedback'></span>");
            $('#' + campo).val("");
            $('#' + campo).focus();
            return false;
        }
        else {
            $("#glypcn" + campo).removeClass('form-group has-error has-feedback');
            $('#' + campo).parent().attr("class", "form-group has-success has-feedback");
            $('#' + campo).parent().children('span').hide();
            $('#' + campo).parent().append("<span id='glypcn" + campo + "' class='glyphicon glyphicon-ok form-control-feedback'></span>");
            return true;
        }
    }
    // V A L I D A D C I O N E S  L O C A L I D A D
    if (campo === 'Comunidad') {

        var localidad = $('#Comunidad').val().trim();  //Se obtiene el valor del campo y Se eliminan espacios en blanco con trim()
        if (localidad === '') {
            $("#glypcn" + campo).remove();
            $('#' + campo).parent().attr("class", "form-group has-error has-feedback");
            $('#' + campo).parent().children('span').text("Debes ingresar tu localidad").show();
            $('#' + campo).parent().append("<span id='glypcn" + campo + "' class='glyphicon glyphicon-remove form-control-feedback'></span>");
            $('#' + campo).val("");
            $('#' + campo).focus();
            return false;
        }
        else {
            $("#glypcn" + campo).removeClass('form-group has-error has-feedback');
            $('#' + campo).parent().attr("class", "form-group has-success has-feedback");
            $('#' + campo).parent().children('span').hide();
            $('#' + campo).parent().append("<span id='glypcn" + campo + "' class='glyphicon glyphicon-ok form-control-feedback'></span>");
            return true;
        }
    }
    // V A L I D A D C I O N E S  C A L L E 
    if (campo === 'Calle') {

        var calle = $('#Calle').val().trim();  //Se obtiene el valor del campo y Se eliminan espacios en blanco con trim()
        if (calle === '') {
            $("#glypcn" + campo).remove();
            $('#' + campo).parent().attr("class", "form-group has-error has-feedback");
            $('#' + campo).parent().children('span').text("Debes ingresar tu calle").show();
            $('#' + campo).parent().append("<span id='glypcn" + campo + "' class='glyphicon glyphicon-remove form-control-feedback'></span>");
            $('#' + campo).val("");
            $('#' + campo).focus();
            return false;
        }
        else {
            $("#glypcn" + campo).removeClass('form-group has-error has-feedback');
            $('#' + campo).parent().attr("class", "form-group has-success has-feedback");
            $('#' + campo).parent().children('span').hide();
            $('#' + campo).parent().append("<span id='glypcn" + campo + "' class='glyphicon glyphicon-ok form-control-feedback'></span>");
            return true;
        }
    }

    // V A L I D A D C I O N E S  C O L O N I A
    if (campo === 'Colonia') {

        var colonia = $('#Colonia').val().trim();  //Se obtiene el valor del campo y Se eliminan espacios en blanco con trim()
        if (colonia === '') {
            $("#glypcn" + campo).remove();
            $('#' + campo).parent().attr("class", "form-group has-error has-feedback");
            $('#' + campo).parent().children('span').text("Debes ingresar tu colonia").show();
            $('#' + campo).parent().append("<span id='glypcn" + campo + "' class='glyphicon glyphicon-remove form-control-feedback'></span>");
            $('#' + campo).val("");
            $('#' + campo).focus();
            return false;
        }
        else {
            $("#glypcn" + campo).removeClass('form-group has-error has-feedback');
            $('#' + campo).parent().attr("class", "form-group has-success has-feedback");
            $('#' + campo).parent().children('span').hide();
            $('#' + campo).parent().append("<span id='glypcn" + campo + "' class='glyphicon glyphicon-ok form-control-feedback'></span>");
            return true;
        }
    }

    // V A L I D A D C I O N E S  C A R R E R A

    if (campo === 'Carrera') {
        indice = document.getElementById(campo).selectedIndex;
        if (indice === null || indice === 0) {
            $("#glypcn" + campo).remove();
            $('#' + campo).parent().attr("class", "form-group has-error has-feedback");
            $('#' + campo).parent().children('span').text("Debes seleccionar tu carrera").show();
            $('#' + campo).focus();
            return false;
        }
        else {
            $('#' + campo).parent().attr("class", "form-group has-success");
            $('#' + campo).parent().children('span').hide();
            return true;

        }
    }
    // V A L I D A D C I O N E S   U N I V E R S I D A D

    if (campo === 'Universidad') {
        indice = document.getElementById(campo).selectedIndex;
        if (indice === null || indice === 0) {
            $("#glypcn" + campo).remove();
            $('#' + campo).parent().attr("class", "form-group has-error has-feedback");
            $('#' + campo).parent().children('span').text("Debes seleccionar tu Universidad").show();
            $('#' + campo).focus();
            return false;
        }
        else {
            $('#' + campo).parent().attr("class", "form-group has-success");
            $('#' + campo).parent().children('span').hide();
            return true;
        }
    }
    // V A L I D A D C I O N E S  A R E A 

    if (campo === 'Area') {
        indice = document.getElementById(campo).selectedIndex;
        if (indice === null || indice === 0) {
            $("#glypcn" + campo).remove();
            $('#' + campo).parent().attr("class", "form-group has-error has-feedback");
            $('#' + campo).parent().children('span').text("Debes seleccionar tu area").show();
            $('#' + campo).focus();
            return false;
        }
        else {
            $('#' + campo).parent().attr("class", "form-group has-success");
            $('#' + campo).parent().children('span').hide();
        }
    }
    // V A L I D A D C I O N E S  C U A T R I M E S T R E

    if (campo === 'Cuatrimestre') {
        indice = document.getElementById(campo).selectedIndex;
        if (indice === null || indice === 0) {
            $("#glypcn" + campo).remove();
            $('#' + campo).parent().attr("class", "form-group has-error has-feedback");
            $('#' + campo).parent().children('span').text("Debes seleccionar tu cuatrimestre").show();
            $('#' + campo).focus();
            return false;
        }
        else {
            $('#' + campo).parent().attr("class", "form-group has-success");
            $('#' + campo).parent().children('span').hide();
        }
    }

  
    // V A L I D A D C I O N E S  EDO RESIDENCIA

    if (campo === 'estado-residencia') {
        indice = document.getElementById(campo).selectedIndex;
        if (indice === null || indice === 0) {
            $("#glypcn" + campo).remove();
            $('#' + campo).parent().attr("class", "form-group has-error has-feedback");
            $('#' + campo).parent().children('span').text("Debes seleccionar tu estado").show();
            $('#' + campo).focus();
            return false;
        }
        else {
            $('#' + campo).parent().attr("class", "form-group has-success");
            $('#' + campo).parent().children('span').hide();
            return true;

        }
    }
    // V A L I D A D C I O N E S  MUNICIPIO RESIDENCIA

    if (campo === 'municipio-residencia') {
        indice = document.getElementById(campo).selectedIndex;
        if (indice === null || indice === 0) {
            $("#glypcn" + campo).remove();
            $('#' + campo).parent().attr("class", "form-group has-error has-feedback");
            $('#' + campo).parent().children('span').text("Debes seleccionar tu municipio").show();
            $('#' + campo).focus();
            return false;
        }
        else {
            $('#' + campo).parent().attr("class", "form-group has-success");
            $('#' + campo).parent().children('span').hide();
            return true;
        }
    }
  
}


function enviarDatos() {
    var datos = $('#RegistroAlumno').serializeArray();
    $.ajax({
        url: 'InsertarAlumno.php',
        type: 'POST',
        data: datos,
        success: function (data) {
            $('body').css("overflow", "auto");


            colocarAlerta(4, data);

            $('#adelante').click(function () {
                $("#overlay").hide();
                $("#Registro").hide();
                $('body').css("overflow", "auto");
                $('#RegistroAlumno').trigger("reset");

                $("#inicio").fadeIn();
            });
             $('#RegistroAlumno').trigger("reset");
             $('#wrap').css("height", "75%");

        }
    });
}



function colocarAlerta(tipo, mensaje)
{

    var banderaTipo = '';
    if (tipo === 1)
    {
        banderaTipo = 'alert-warning';
    }
    else if (tipo === 2)
    {
        banderaTipo = 'alert-danger';
    }
    if (tipo < 3) {
        var bandera = '  <div class="alert ' + banderaTipo + ' alert-dismissible fade in" role="alert"> ' +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>' +
                '<strong>' + mensaje + '</strong></div>';
        $('#bandera').html(bandera).slideDown('slow');
    }

    if (tipo === 3) {
        banderaTipo = 'alert-success';
        var bandera = '  <div class="alert ' + banderaTipo + ' alert-dismissible fade in" role="alert"> ' +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>' +
                '<strong>' + mensaje + '</strong></div>';
        $('#bandera').html(bandera).slideDown('slow');
    }
    if (tipo === 4) {

        $('#bandera').html(mensaje).slideDown('slow');
    }

}


$("#btn-Registro").click(function () {
    $("#inicio").hide();
    $("#Registro").show();

});


$("#btn-Consultar").click(function () {
    actualizarRegistros();
    $("#inicio").hide();
    $("#ConsultarRegistros").show();

});

$(".backbtn").click(function () {
    $("#Registro").hide();
    $("#ConsultarRegistros").hide();
    $("#inicio").fadeIn();
}
);

function actualizarRegistros() {

    $.ajax({
        url: 'Actualizar_Registros.php',
        data: 'registros=' + 1,
        type: 'POST',
        success: function (data) {
            $('#tableReg tbody').html(data);
        }
    });
}

function cambiarMunicipio(idEstado)
{
    $.ajax({
        url: 'Agregar_Municipio.php',
        data: 'idEstado=' + idEstado,
        type: 'POST',
        success: function (data) {
            $('#municipio-residencia').html(' <option  selected="selected" disabled="" >Municipio</option>' + data);
        }
    });
}




function cambiarCarreras(idUniversidad)
{
    $.ajax({
        url: 'Agregar_Carrera.php',
        data: 'idUniversidad=' + idUniversidad,
        type: 'POST',
        success: function (data) {
            $('#Carrera').html(' <option  selected="selected" disabled="" >Carrera/Division</option>' + data);
        }
    });
}


function cambiarAreas(idCarrera)
{
    $.ajax({
        url: 'Agregar_Area.php',
        data: 'idCarrera=' + idCarrera,
        type: 'POST',
        success: function (data) {
            if (data === 'NADA') {
                $('#Area').html(' <option value"N/A"  selected="selected"  disabled="">Area/Facultad (N/A)</option>' + data);
                $('#Area').attr("disabled", "disabled");

            }
            else {
                $('#Area').html(' <option  selected="selected" disabled="">Area/Facultad</option>' + data);
                $('#Area').removeAttr("disabled", "disabled");
            }
        }
    });
}


function perfilAlumnoVoc(id) {
    var datos = {'id': id};
    $.ajax({
        url: 'perfilAlumno.php',
        type: 'POST',
        data: datos,
        success: function (data) {

            $('#modalPerfill .modal-body').html(data);

        }
    });
}








    